--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

-- Started on 2023-05-21 03:55:34

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2937 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 199 (class 1259 OID 149651)
-- Name: ciudad; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE ciudad (
    id_ciu bigint NOT NULL,
    nombre character varying(15) NOT NULL,
    descripcion character varying(50) NOT NULL
);


ALTER TABLE ciudad OWNER TO ingeneobd;

--
-- TOC entry 212 (class 1259 OID 149816)
-- Name: sec_emp; Type: SEQUENCE; Schema: public; Owner: ingeneobd
--

CREATE SEQUENCE sec_emp
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999999
    CACHE 1;


ALTER TABLE sec_emp OWNER TO ingeneobd;

--
-- TOC entry 202 (class 1259 OID 149666)
-- Name: empresa; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE empresa (
    id_emp bigint DEFAULT nextval('sec_emp'::regclass) NOT NULL,
    nit character varying(40) NOT NULL,
    nombre character varying(40) NOT NULL,
    correo character varying(50) NOT NULL,
    direccion character varying(50) NOT NULL,
    telefono character varying(50) NOT NULL
);


ALTER TABLE empresa OWNER TO ingeneobd;

--
-- TOC entry 210 (class 1259 OID 149771)
-- Name: flota; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE flota (
    id_mtr bigint NOT NULL,
    numero_flota character varying(8) NOT NULL
);


ALTER TABLE flota OWNER TO ingeneobd;

--
-- TOC entry 215 (class 1259 OID 149824)
-- Name: sec_logenv; Type: SEQUENCE; Schema: public; Owner: ingeneobd
--

CREATE SEQUENCE sec_logenv
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE sec_logenv OWNER TO ingeneobd;

--
-- TOC entry 211 (class 1259 OID 149781)
-- Name: logistica_envio; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE logistica_envio (
    id_env bigint DEFAULT nextval('sec_logenv'::regclass) NOT NULL,
    id_sol bigint NOT NULL,
    id_mtr bigint NOT NULL,
    id_lue bigint,
    fecha_entrega date,
    numero_guia character varying(10) NOT NULL,
    precio_total double precision
);


ALTER TABLE logistica_envio OWNER TO ingeneobd;

--
-- TOC entry 208 (class 1259 OID 149746)
-- Name: lugar_entrega; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE lugar_entrega (
    id_lue bigint NOT NULL,
    id_tlu bigint NOT NULL,
    id_ciu bigint NOT NULL,
    nombre character varying(15) NOT NULL,
    direccion character varying(15) NOT NULL,
    id_ttr bigint NOT NULL
);


ALTER TABLE lugar_entrega OWNER TO ingeneobd;

--
-- TOC entry 207 (class 1259 OID 149736)
-- Name: medio_transporte; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE medio_transporte (
    id_mtr bigint NOT NULL,
    id_ttr bigint NOT NULL,
    descripcion character varying(50)
);


ALTER TABLE medio_transporte OWNER TO ingeneobd;

--
-- TOC entry 214 (class 1259 OID 149822)
-- Name: sec_solenv; Type: SEQUENCE; Schema: public; Owner: ingeneobd
--

CREATE SEQUENCE sec_solenv
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE sec_solenv OWNER TO ingeneobd;

--
-- TOC entry 213 (class 1259 OID 149819)
-- Name: sec_tipopro; Type: SEQUENCE; Schema: public; Owner: ingeneobd
--

CREATE SEQUENCE sec_tipopro
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 999999;


ALTER TABLE sec_tipopro OWNER TO ingeneobd;

--
-- TOC entry 203 (class 1259 OID 149671)
-- Name: solicitud_envio; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE solicitud_envio (
    id_sol bigint DEFAULT nextval('sec_solenv'::regclass) NOT NULL,
    id_emp bigint NOT NULL,
    id_ttr bigint NOT NULL,
    fecha_solicitud date NOT NULL,
    observaciones character varying(500) NOT NULL
);


ALTER TABLE solicitud_envio OWNER TO ingeneobd;

--
-- TOC entry 206 (class 1259 OID 149721)
-- Name: solicitud_producto; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE solicitud_producto (
    id_pro bigint NOT NULL,
    id_sol bigint NOT NULL,
    cantidad bigint NOT NULL,
    precio double precision NOT NULL,
    precio_descuento double precision,
    porcentaje_descuento double precision
);


ALTER TABLE solicitud_producto OWNER TO ingeneobd;

--
-- TOC entry 198 (class 1259 OID 149646)
-- Name: tipo_lugar; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE tipo_lugar (
    id_tlu bigint NOT NULL,
    nombre character varying(15) NOT NULL,
    descripcion character varying(50) NOT NULL
);


ALTER TABLE tipo_lugar OWNER TO ingeneobd;

--
-- TOC entry 200 (class 1259 OID 149656)
-- Name: tipo_producto; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE tipo_producto (
    id_pro bigint DEFAULT nextval('sec_tipopro'::regclass) NOT NULL,
    nombre character varying(15) NOT NULL,
    descripcion character varying(50) NOT NULL,
    precio_envio double precision NOT NULL
);


ALTER TABLE tipo_producto OWNER TO ingeneobd;

--
-- TOC entry 196 (class 1259 OID 149631)
-- Name: tipo_rol; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE tipo_rol (
    id_rol bigint NOT NULL,
    nombre character varying(15) NOT NULL,
    descripcion character varying(50) NOT NULL
);


ALTER TABLE tipo_rol OWNER TO ingeneobd;

--
-- TOC entry 197 (class 1259 OID 149636)
-- Name: tipo_transporte; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE tipo_transporte (
    id_ttr bigint NOT NULL,
    nombre character varying(15) NOT NULL,
    descripcion character varying(50) NOT NULL
);


ALTER TABLE tipo_transporte OWNER TO ingeneobd;

--
-- TOC entry 201 (class 1259 OID 149661)
-- Name: usuario; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE usuario (
    id_usu bigint NOT NULL,
    identificacion character varying(50) NOT NULL,
    nombres character varying(50) NOT NULL,
    apellidos character varying(50) NOT NULL,
    correo character varying(50) NOT NULL,
    celular character varying(50) NOT NULL,
    usuario character varying(20) NOT NULL,
    contrasena character varying(50) NOT NULL
);


ALTER TABLE usuario OWNER TO ingeneobd;

--
-- TOC entry 205 (class 1259 OID 149706)
-- Name: usuario_empresa; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE usuario_empresa (
    id_usu bigint NOT NULL,
    id_emp bigint NOT NULL
);


ALTER TABLE usuario_empresa OWNER TO ingeneobd;

--
-- TOC entry 204 (class 1259 OID 149691)
-- Name: usuario_rol; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE usuario_rol (
    id_usu bigint NOT NULL,
    id_rol bigint NOT NULL
);


ALTER TABLE usuario_rol OWNER TO ingeneobd;

--
-- TOC entry 209 (class 1259 OID 149761)
-- Name: vehiculo; Type: TABLE; Schema: public; Owner: ingeneobd
--

CREATE TABLE vehiculo (
    id_mtr bigint NOT NULL,
    placa character varying(6) NOT NULL,
    modelo character varying(20) NOT NULL,
    marca character varying(20) NOT NULL
);


ALTER TABLE vehiculo OWNER TO ingeneobd;

--
-- TOC entry 2914 (class 0 OID 149651)
-- Dependencies: 199
-- Data for Name: ciudad; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY ciudad (id_ciu, nombre, descripcion) FROM stdin;
2	CALI	CIUDAD DE COLOMBIA
1	BOGOTÁ D.C	CIUDAD DE COLOMBIA
4	BARRANQUILLA	CIUDAD DE COLOMBIA
3	MEDELLIN	CIUDAD DE COLOMBIA
\.


--
-- TOC entry 2917 (class 0 OID 149666)
-- Dependencies: 202
-- Data for Name: empresa; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY empresa (id_emp, nit, nombre, correo, direccion, telefono) FROM stdin;
\.


--
-- TOC entry 2925 (class 0 OID 149771)
-- Dependencies: 210
-- Data for Name: flota; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY flota (id_mtr, numero_flota) FROM stdin;
1	FGR1498A
\.


--
-- TOC entry 2926 (class 0 OID 149781)
-- Dependencies: 211
-- Data for Name: logistica_envio; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY logistica_envio (id_env, id_sol, id_mtr, id_lue, fecha_entrega, numero_guia, precio_total) FROM stdin;
\.


--
-- TOC entry 2923 (class 0 OID 149746)
-- Dependencies: 208
-- Data for Name: lugar_entrega; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY lugar_entrega (id_lue, id_tlu, id_ciu, nombre, direccion, id_ttr) FROM stdin;
\.


--
-- TOC entry 2922 (class 0 OID 149736)
-- Dependencies: 207
-- Data for Name: medio_transporte; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY medio_transporte (id_mtr, id_ttr, descripcion) FROM stdin;
2	2	MEDIO TERRESTRE
1	1	MEDIO MARITIMO
\.


--
-- TOC entry 2918 (class 0 OID 149671)
-- Dependencies: 203
-- Data for Name: solicitud_envio; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY solicitud_envio (id_sol, id_emp, id_ttr, fecha_solicitud, observaciones) FROM stdin;
\.


--
-- TOC entry 2921 (class 0 OID 149721)
-- Dependencies: 206
-- Data for Name: solicitud_producto; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY solicitud_producto (id_pro, id_sol, cantidad, precio, precio_descuento, porcentaje_descuento) FROM stdin;
\.


--
-- TOC entry 2913 (class 0 OID 149646)
-- Dependencies: 198
-- Data for Name: tipo_lugar; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY tipo_lugar (id_tlu, nombre, descripcion) FROM stdin;
\.


--
-- TOC entry 2915 (class 0 OID 149656)
-- Dependencies: 200
-- Data for Name: tipo_producto; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY tipo_producto (id_pro, nombre, descripcion, precio_envio) FROM stdin;
\.


--
-- TOC entry 2911 (class 0 OID 149631)
-- Dependencies: 196
-- Data for Name: tipo_rol; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY tipo_rol (id_rol, nombre, descripcion) FROM stdin;
2	CLIENTE	CONSULTA NUMEROS DE GUIA
1	ADMINISTRADOR	CREA CLIENTES E INGRESA PRODUCTOS
\.


--
-- TOC entry 2912 (class 0 OID 149636)
-- Dependencies: 197
-- Data for Name: tipo_transporte; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY tipo_transporte (id_ttr, nombre, descripcion) FROM stdin;
2	TERRESTRE	TRANSPORTE POR TIERRA
1	MARITIMO	TRANSPORTE POR MAR
3	AÉREO	TRANSPORTE POR AIRE
\.


--
-- TOC entry 2916 (class 0 OID 149661)
-- Dependencies: 201
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY usuario (id_usu, identificacion, nombres, apellidos, correo, celular, usuario, contrasena) FROM stdin;
1	233231	WILLIAN ANDRES	PIARPUZAN ARTUNDUAGA	andress0909@gmail.com	3128099822	andress0909	123
\.


--
-- TOC entry 2920 (class 0 OID 149706)
-- Dependencies: 205
-- Data for Name: usuario_empresa; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY usuario_empresa (id_usu, id_emp) FROM stdin;
\.


--
-- TOC entry 2919 (class 0 OID 149691)
-- Dependencies: 204
-- Data for Name: usuario_rol; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY usuario_rol (id_usu, id_rol) FROM stdin;
1	1
1	2
\.


--
-- TOC entry 2924 (class 0 OID 149761)
-- Dependencies: 209
-- Data for Name: vehiculo; Type: TABLE DATA; Schema: public; Owner: ingeneobd
--

COPY vehiculo (id_mtr, placa, modelo, marca) FROM stdin;
2	ASD147	ACTROS	MERCEDES BENZ
\.


--
-- TOC entry 2938 (class 0 OID 0)
-- Dependencies: 212
-- Name: sec_emp; Type: SEQUENCE SET; Schema: public; Owner: ingeneobd
--

SELECT pg_catalog.setval('sec_emp', 1, false);


--
-- TOC entry 2939 (class 0 OID 0)
-- Dependencies: 215
-- Name: sec_logenv; Type: SEQUENCE SET; Schema: public; Owner: ingeneobd
--

SELECT pg_catalog.setval('sec_logenv', 1, false);


--
-- TOC entry 2940 (class 0 OID 0)
-- Dependencies: 214
-- Name: sec_solenv; Type: SEQUENCE SET; Schema: public; Owner: ingeneobd
--

SELECT pg_catalog.setval('sec_solenv', 1, false);


--
-- TOC entry 2941 (class 0 OID 0)
-- Dependencies: 213
-- Name: sec_tipopro; Type: SEQUENCE SET; Schema: public; Owner: ingeneobd
--

SELECT pg_catalog.setval('sec_tipopro', 1, false);


--
-- TOC entry 2754 (class 2606 OID 149670)
-- Name: empresa emp_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY empresa
    ADD CONSTRAINT emp_pkey PRIMARY KEY (id_emp);


--
-- TOC entry 2770 (class 2606 OID 149775)
-- Name: flota flota_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY flota
    ADD CONSTRAINT flota_pkey PRIMARY KEY (id_mtr);


--
-- TOC entry 2772 (class 2606 OID 149785)
-- Name: logistica_envio logistica_envio_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY logistica_envio
    ADD CONSTRAINT logistica_envio_pkey PRIMARY KEY (id_env);


--
-- TOC entry 2766 (class 2606 OID 149750)
-- Name: lugar_entrega lugar_entrega_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY lugar_entrega
    ADD CONSTRAINT lugar_entrega_pkey PRIMARY KEY (id_lue);


--
-- TOC entry 2764 (class 2606 OID 149740)
-- Name: medio_transporte medio_transporte_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY medio_transporte
    ADD CONSTRAINT medio_transporte_pkey PRIMARY KEY (id_mtr);


--
-- TOC entry 2756 (class 2606 OID 149678)
-- Name: solicitud_envio solicitud_envio_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY solicitud_envio
    ADD CONSTRAINT solicitud_envio_pkey PRIMARY KEY (id_sol);


--
-- TOC entry 2762 (class 2606 OID 149725)
-- Name: solicitud_producto solicitud_producto_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY solicitud_producto
    ADD CONSTRAINT solicitud_producto_pkey PRIMARY KEY (id_pro, id_sol);


--
-- TOC entry 2748 (class 2606 OID 149655)
-- Name: ciudad tipo_ciu_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY ciudad
    ADD CONSTRAINT tipo_ciu_pkey PRIMARY KEY (id_ciu);


--
-- TOC entry 2750 (class 2606 OID 149660)
-- Name: tipo_producto tipo_pro_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY tipo_producto
    ADD CONSTRAINT tipo_pro_pkey PRIMARY KEY (id_pro);


--
-- TOC entry 2742 (class 2606 OID 149635)
-- Name: tipo_rol tipo_rol_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY tipo_rol
    ADD CONSTRAINT tipo_rol_pkey PRIMARY KEY (id_rol);


--
-- TOC entry 2746 (class 2606 OID 149650)
-- Name: tipo_lugar tipo_tlu_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY tipo_lugar
    ADD CONSTRAINT tipo_tlu_pkey PRIMARY KEY (id_tlu);


--
-- TOC entry 2744 (class 2606 OID 149685)
-- Name: tipo_transporte tipo_tra_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY tipo_transporte
    ADD CONSTRAINT tipo_tra_pkey PRIMARY KEY (id_ttr);


--
-- TOC entry 2752 (class 2606 OID 149665)
-- Name: usuario usu_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usu_pkey PRIMARY KEY (id_usu);


--
-- TOC entry 2760 (class 2606 OID 149710)
-- Name: usuario_empresa usuario_empresa_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY usuario_empresa
    ADD CONSTRAINT usuario_empresa_pkey PRIMARY KEY (id_usu, id_emp);


--
-- TOC entry 2758 (class 2606 OID 149695)
-- Name: usuario_rol usuario_rol_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY usuario_rol
    ADD CONSTRAINT usuario_rol_pkey PRIMARY KEY (id_usu, id_rol);


--
-- TOC entry 2768 (class 2606 OID 149765)
-- Name: vehiculo vehiculo_pkey; Type: CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY vehiculo
    ADD CONSTRAINT vehiculo_pkey PRIMARY KEY (id_mtr);


--
-- TOC entry 2789 (class 2606 OID 149796)
-- Name: logistica_envio fk_env_lue; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY logistica_envio
    ADD CONSTRAINT fk_env_lue FOREIGN KEY (id_lue) REFERENCES lugar_entrega(id_lue);


--
-- TOC entry 2788 (class 2606 OID 149791)
-- Name: logistica_envio fk_env_mtr; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY logistica_envio
    ADD CONSTRAINT fk_env_mtr FOREIGN KEY (id_mtr) REFERENCES medio_transporte(id_mtr);


--
-- TOC entry 2787 (class 2606 OID 149786)
-- Name: logistica_envio fk_env_sol; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY logistica_envio
    ADD CONSTRAINT fk_env_sol FOREIGN KEY (id_sol) REFERENCES solicitud_envio(id_sol);


--
-- TOC entry 2786 (class 2606 OID 149776)
-- Name: flota fk_flo_mtr; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY flota
    ADD CONSTRAINT fk_flo_mtr FOREIGN KEY (id_mtr) REFERENCES medio_transporte(id_mtr);


--
-- TOC entry 2783 (class 2606 OID 149756)
-- Name: lugar_entrega fk_lue_ciu; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY lugar_entrega
    ADD CONSTRAINT fk_lue_ciu FOREIGN KEY (id_ciu) REFERENCES ciudad(id_ciu);


--
-- TOC entry 2782 (class 2606 OID 149751)
-- Name: lugar_entrega fk_lue_tlu; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY lugar_entrega
    ADD CONSTRAINT fk_lue_tlu FOREIGN KEY (id_tlu) REFERENCES tipo_lugar(id_tlu);


--
-- TOC entry 2784 (class 2606 OID 149811)
-- Name: lugar_entrega fk_lue_ttr; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY lugar_entrega
    ADD CONSTRAINT fk_lue_ttr FOREIGN KEY (id_ttr) REFERENCES tipo_transporte(id_ttr);


--
-- TOC entry 2781 (class 2606 OID 149806)
-- Name: medio_transporte fk_medtran_ttr; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY medio_transporte
    ADD CONSTRAINT fk_medtran_ttr FOREIGN KEY (id_ttr) REFERENCES tipo_transporte(id_ttr);


--
-- TOC entry 2773 (class 2606 OID 149679)
-- Name: solicitud_envio fk_sol_emp; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY solicitud_envio
    ADD CONSTRAINT fk_sol_emp FOREIGN KEY (id_emp) REFERENCES empresa(id_emp);


--
-- TOC entry 2774 (class 2606 OID 149686)
-- Name: solicitud_envio fk_sol_ttr; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY solicitud_envio
    ADD CONSTRAINT fk_sol_ttr FOREIGN KEY (id_ttr) REFERENCES tipo_transporte(id_ttr);


--
-- TOC entry 2780 (class 2606 OID 149731)
-- Name: solicitud_producto fk_solpro_pro; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY solicitud_producto
    ADD CONSTRAINT fk_solpro_pro FOREIGN KEY (id_pro) REFERENCES tipo_producto(id_pro);


--
-- TOC entry 2779 (class 2606 OID 149726)
-- Name: solicitud_producto fk_solpro_sol; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY solicitud_producto
    ADD CONSTRAINT fk_solpro_sol FOREIGN KEY (id_sol) REFERENCES solicitud_envio(id_sol);


--
-- TOC entry 2777 (class 2606 OID 149711)
-- Name: usuario_empresa fk_usuemp_emp; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY usuario_empresa
    ADD CONSTRAINT fk_usuemp_emp FOREIGN KEY (id_emp) REFERENCES empresa(id_emp);


--
-- TOC entry 2778 (class 2606 OID 149716)
-- Name: usuario_empresa fk_usuemp_usu; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY usuario_empresa
    ADD CONSTRAINT fk_usuemp_usu FOREIGN KEY (id_usu) REFERENCES usuario(id_usu);


--
-- TOC entry 2776 (class 2606 OID 149701)
-- Name: usuario_rol fk_usurol_rol; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY usuario_rol
    ADD CONSTRAINT fk_usurol_rol FOREIGN KEY (id_rol) REFERENCES tipo_rol(id_rol);


--
-- TOC entry 2775 (class 2606 OID 149696)
-- Name: usuario_rol fk_usurol_usu; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY usuario_rol
    ADD CONSTRAINT fk_usurol_usu FOREIGN KEY (id_usu) REFERENCES usuario(id_usu);


--
-- TOC entry 2785 (class 2606 OID 149766)
-- Name: vehiculo fk_veh_mtr; Type: FK CONSTRAINT; Schema: public; Owner: ingeneobd
--

ALTER TABLE ONLY vehiculo
    ADD CONSTRAINT fk_veh_mtr FOREIGN KEY (id_mtr) REFERENCES medio_transporte(id_mtr);


-- Completed on 2023-05-21 03:55:37

--
-- PostgreSQL database dump complete
--

