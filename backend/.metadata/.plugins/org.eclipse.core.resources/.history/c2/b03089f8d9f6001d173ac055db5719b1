package com.ingeneo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ingeneo.model.Empresa;
import com.ingeneo.model.TipoRol;
import com.ingeneo.model.Usuario;
import com.ingeneo.repository.EmpresaRepository;
import com.ingeneo.repository.UsuarioRepository;
import com.ingeneo.request.EmpresaRequest;
import com.ingeneo.request.UsuarioRequest;
import com.ingeneo.response.UsuarioResponse;
import com.ingeneo.EstadoTransaccion;
import com.ingeneo.exception.BadRequestException;
import com.ingeneo.exception.NotFoundException;
import com.ingeneo.response.EmpresaResponse;

@Service
public class EmpresaService {

	@Autowired
	EmpresaRepository empresaRepository;

	// Metodo que inserta los datos de la empresa en la BD
	public ResponseEntity<List<EmpresaResponse>> crear(EmpresaRequest empresaRqs) {
		if (!EmpresaRequest.isNull(empresaRqs)) {
			EmpresaResponse empresaRes = new EmpresaResponse();
			// Asignan datos al model para realizar la insercion en la BD
			Empresa empresa = new Empresa();
			empresa.setNit(empresaRqs.getNit());
			empresa.setNombre(empresaRqs.getNombre());
			empresa.setCorreo(empresaRqs.getCorreo());
			empresa.setDireccion(empresaRqs.getDireccion());
			empresa.setTelefono(empresaRqs.getTelefono());

			empresaRepository.save(empresa);

			usuarioRes.setIdUsu(usuario.getIdUsu());
			usuarioRes.setIdentificacion(usuario.getIdentificacion());
			usuarioRes.setNombres(usuario.getNombres());
			usuarioRes.setApellidos(usuario.getApellidos());
			usuarioRes.setCorreo(usuario.getCorreo());
			usuarioRes.setCelular(usuario.getCelular());
			usuarioRes.setUsuario(usuario.getUsuario());

			List<EmpresaResponse> list = new ArrayList<EmpresaResponse>();
			list.add(empresaRes);
			return ResponseEntity.ok(list);
		} else {
			throw new BadRequestException(EstadoTransaccion.USUARIO_VACIO.getDescripcion());
		}
	}

	// Metodo que valida el usuario y la contraseña
	public ResponseEntity<List<UsuarioResponse>> getLogin(UsuarioRequest usuarioRqs) {
		if (usuarioRqs.getUsuario() != null && usuarioRqs.getContrasena() != null) {
			if (!(usuarioRqs.getUsuario().equals("") || usuarioRqs.getContrasena().equals(""))) {
				Usuario usuario = usuarioRepository.findByUsuarioUsu(usuarioRqs.getUsuario());
				if (usuario != null) {
					// Verificando password con la BD
					BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12); // Strength set as 12
					if (encoder.matches(usuarioRqs.getContrasena(), usuario.getContrasena())) {
						UsuarioResponse usuarioRes = new UsuarioResponse();
						usuarioRes.setIdUsu(usuario.getIdUsu());
						usuarioRes.setIdentificacion(usuario.getIdentificacion());
						usuarioRes.setNombres(usuario.getNombres());
						usuarioRes.setApellidos(usuario.getApellidos());
						usuarioRes.setCorreo(usuario.getCorreo());
						usuarioRes.setCelular(usuario.getCelular());
						usuarioRes.setUsuario(usuario.getUsuario());

						List<String> roles = new ArrayList<String>();
						// Recorriendo perfiles
						for (TipoRol trol : usuario.getTipoRolList()) {
							roles.add(trol.getNombre());
						}
						usuarioRes.setRoles(roles);

						List<UsuarioResponse> list = new ArrayList<UsuarioResponse>();
						list.add(usuarioRes);
						return ResponseEntity.ok(list);
					} else {
						throw new BadRequestException(
								EstadoTransaccion.CREDENCIALES_INVALIDAS.getDescripcion() + usuarioRqs.getUsuario());
					}
				} else {
					throw new NotFoundException(
							EstadoTransaccion.USUARIO_NO_ENCONTRADO.getDescripcion() + usuarioRqs.getUsuario());
				}
			} else {
				throw new BadRequestException(EstadoTransaccion.USUARIO_VACIO.getDescripcion());
			}
		} else {
			throw new BadRequestException(EstadoTransaccion.USUARIO_VACIO.getDescripcion());
		}
	}
}
