package com.ingeneo.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ingeneo.EstadoTransaccion;
import com.ingeneo.exception.BadRequestException;
import com.ingeneo.model.LogisticaEnvio;
import com.ingeneo.model.SolicitudEnvio;
import com.ingeneo.model.SolicitudProducto;
import com.ingeneo.model.SolicitudProductoPK;
import com.ingeneo.model.TipoProducto;
import com.ingeneo.repository.EmpresaRepository;
import com.ingeneo.repository.LogisticaEnvioRepository;
import com.ingeneo.repository.MedioTransporteRepository;
import com.ingeneo.repository.SolicitudEnvioRepository;
import com.ingeneo.repository.SolicitudProductoRepository;
import com.ingeneo.repository.TipoProductoRepository;
import com.ingeneo.repository.TipoTransporteRepository;
import com.ingeneo.request.SolicitudProductoRequest;
import com.ingeneo.request.SolicitudRequest;
import com.ingeneo.request.TipoProductoRequest;
import com.ingeneo.response.SolicitudResponse;
import com.ingeneo.response.TipoProductoResponse;

@Service
public class LogisticaService {

	public static SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	public static String MARITIMO = "MARITIMO";
	public static String TERRESTRE = "TERRESTRE";

	@Autowired
	SolicitudEnvioRepository solicitudERepository;

	@Autowired
	LogisticaEnvioRepository logisticaERepository;

	@Autowired
	EmpresaRepository empresaRepository;

	@Autowired
	TipoTransporteRepository tipoTRepository;

	@Autowired
	TipoProductoRepository tipoPRepository;
	
	@Autowired
	SolicitudProductoRepository solicitudPRepository;
	
	@Autowired
	MedioTransporteRepository medioTRepository;

	@Transactional
	// Metodo que inserta los datos de la solicitud en la BD
	public ResponseEntity<List<SolicitudResponse>> crear(SolicitudRequest solicitudRqs) {
		if (!SolicitudRequest.isNull(solicitudRqs)) {
			SolicitudResponse solicitudRes = new SolicitudResponse();
			// Se realizaran 3 pasos para generar la funcionalidad de gestión logistica
			// terrestre o maritima
			// Paso 1: Se crea la solicitud general de envio
			SolicitudEnvio solicitudE = new SolicitudEnvio();

			if ((empresaRepository.existsById(solicitudRqs.getIdEmp())) ? false : true)
				throw new BadRequestException(EstadoTransaccion.EMPRESA_NO_EXISTE.getDescripcion());

			solicitudE.setIdEmp(empresaRepository.getOne(solicitudRqs.getIdEmp()));

			if ((tipoTRepository.existsById(solicitudRqs.getIdTtr())) ? false : true)
				throw new BadRequestException(EstadoTransaccion.TIPO_TRANSPORTE_NO_EXISTE.getDescripcion());

			solicitudE.setIdTtr(tipoTRepository.getOne(solicitudRqs.getIdTtr()));
			solicitudE.setFechaSolicitud(formato.parse(formato.format(GregorianCalendar.getInstance().getTime())));
			solicitudE.setObservaciones("Se crea solicitud de envio.");

			solicitudERepository.save(solicitudE);

			// Paso 2: Se crean los productos que estaran dentro de la solicitud con
			// cantidades para validar si dará un descuento

			SolicitudProducto solicitudP = new SolicitudProducto();
			Double totalPrecio = 0.0;
			for (SolicitudProductoRequest pro : solicitudRqs.getProductos()) {
				if ((tipoPRepository.existsById(pro.getIdPro())) ? false : true)
					throw new BadRequestException(EstadoTransaccion.TIPO_TRANSPORTE_NO_EXISTE.getDescripcion());

				SolicitudProductoPK pk = new SolicitudProductoPK();
				pk.setIdPro(pro.getIdPro());
				pk.setIdSol(solicitudE.getIdSol());
				solicitudP.setSolicitudProductoPK(pk);
				solicitudP.setSolicitudEnvio(solicitudE);
				solicitudP.setCantidad(pro.getCantidad());
				solicitudP.setPrecio(tipoPRepository.getOne(pro.getIdPro()).getPrecioEnvio());
				solicitudP.setPorcentajeDescuento(0.0);

				if (pro.getCantidad() > 10) {
					switch (solicitudE.getIdTtr().getNombre()) {
					case MARITIMO:
						solicitudP.setPorcentajeDescuento(3.0);
						break;
					case TERRESTRE:
						solicitudP.setPorcentajeDescuento(5.0);
						break;
					}
				}
				solicitudP.setPrecioDescuento(
						solicitudP.getPrecio() - (solicitudP.getPrecio() * solicitudP.getPorcentajeDescuento()));
				solicitudPRepository.save(solicitudP);
			}

			// Paso 3: Se crea la logistica a la solicitud radicada para poder hacer el
			// seguimiento del envio y definir lugar de entrega y medio de transporte
			LogisticaEnvio logisticaE = new LogisticaEnvio();
			logisticaE.setIdSol(solicitudE);
			logisticaE.setIdMtr(medioTRepository.getOne(solicitudRqs.getIdMtr()));
			// Finalmente se envia una respuesta de la gestion logistica completa que se
			// acaba de crear
			List<SolicitudResponse> list = new ArrayList<SolicitudResponse>();
			list.add(productoRes);
			return ResponseEntity.ok(list);
		} else {
			throw new BadRequestException(EstadoTransaccion.SOLICITUD_VACIA.getDescripcion());
		}
	}

}
