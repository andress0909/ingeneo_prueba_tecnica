package com.ingeneo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ingeneo.model.Usuario;
import com.ingeneo.repository.UsuarioRepository;
import com.ingeneo.request.UsuarioRequest;
import com.ingeneo.response.UsuarioResponse;
import com.ingeneo.exception.BadRequestException;
import com.ingeneo.exception.NotFoundException;
import com.ingeneo.response.EmpresaResponse;

@Service
public class UsuarioService {
	
	@Autowired
	UsuarioRepository usuarioRepository;

	//Metodo que inserta los datos del usuario en la BD
	public ResponseEntity<UsuarioResponse> crear(UsuarioRequest usuarioRqs) {
		UsuarioResponse usuarioRes = new UsuarioResponse();
		//Asignan datos al model para realizar la insercion en la BD
		Usuario usuario = new Usuario();
		usuario.setIdentificacion(usuarioRqs.getIdentificacion());
		usuario.setNombres(usuarioRqs.getNombres());
		usuario.setApellidos(usuarioRqs.getApellidos());
		usuario.setCorreo(usuarioRqs.getCorreo());
		usuario.setCelular(usuarioRqs.getCelular());
		usuario.setUsuario(usuarioRqs.getUsuario());
		// encriptando contraseÃ±a
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12); // Strength set as 12
		String encoderPassword = encoder.encode(usuarioRqs.getContrasena());
		usuario.setContrasena(encoderPassword);
		
		usuarioRepository.save(usuario);
		
		return ResponseEntity.ok(usuarioRes);
	}
	
	public ResponseEntity<List<UsuarioResponse>> getLogin(UsuarioRequest usuarioRqs) {
		if (usuarioRqs.getUsuario() != null && usuarioRqs.getContrasena() != null) {
			if (!(usuarioRqs.getUsuario().equals("") || usuarioRqs.getContrasena().equals(""))) {
				Usuario usuario = usuarioRepository.findByUsuarioUsu(usuarioRqs.getUsuario());
				if (usuario != null) {
					// Verificando password con la BD
					BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12); // Strength set as 12
					if (encoder.matches(usuarioRqs.getContrasena(), usuario.getContrasena())) {
						UsuarioResponse usuarioRes = mapper.map(usuario, UsuarioResponse.class);
						List<UsuarioPerfilResponse> perfiles = new ArrayList<UsuarioPerfilResponse>();
						// Recorriendo perfiles
						for (UsuarioCatalogoperfil perfil : usuario.getUsuarioCatalogoperfilList()) {
							UsuarioPerfilResponse perfilRes = mapper.map(perfil, UsuarioPerfilResponse.class);
							perfiles.add(perfilRes);
						}
						usuarioRes.setPerfiles(perfiles);

						EmpresaResponse empresaRes = mapper.map(usuario.getIdEmp(), EmpresaResponse.class);
						usuarioRes.setEmpresa(empresaRes);

						List<UsuarioResponse> list = new ArrayList<UsuarioResponse>();
						list.add(usuarioRes);
						return ResponseEntity.ok(list);
					} else {
						throw new BadRequestException(
								"Credenciales invalidas para usuario: '" + usuarioRqs.getUsuario() + "'");
					}
				} else {
					throw new NotFoundException(
							"Usuario no encontrado en el sistema. Usuario: '" + usuarioRqs.getUsuario() + "'");
				}
			} else {
				throw new BadRequestException("Los parametros 'usuario' y 'contrasena' no deben ser vacios.");
			}
		} else {
			throw new BadRequestException("No se encontraron los parametros 'usuario' y 'contrasena'.");
		}
	}
}
