package com.ingeneo;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.ingeneo.security.JwtAuthorizationFilter;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableWebSecurity
@EnableSwagger2
public class Config extends WebSecurityConfigurerAdapter{	
	//Configuracion JWT- > aqui se llama al filtro del jwt creado para asignar los permisos respectivos,
	//Solo se autoriza ingreso sin restriccion al Servicio Rest que retornar el token (getToken)
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		http.csrf().disable().httpBasic()
		.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and().addFilter(jwtAuthorizationFilter())
		.authorizeRequests()
		.antMatchers(HttpMethod.POST, "/login/getToken").permitAll();
		//.antMatchers("/swagger-ui.html").permitAll()
		//.antMatchers(HttpMethod.POST, "/admin/**").access("hasRole('ADMINISTRADOR')")
		//.antMatchers(HttpMethod.POST, "/logistica/**").access("hasRole('ADMINISTRADOR')")
		//.antMatchers(HttpMethod.GET, "/logistica/**").access("hasRole('ADMINISTRADOR')")
		//.anyRequest().authenticated();
	}
	
	@Bean
	public JwtAuthorizationFilter jwtAuthorizationFilter() throws Exception{
		return new JwtAuthorizationFilter(this.authenticationManager());
	}
	/*Fin Configuracion JWT---------------------------------------------------*/
		
	
	/**SWAGGER**/
	@Bean
	public Docket apiDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.ingeneo.rest"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(getApiInfo())
				;
	}
	
	private ApiInfo getApiInfo() {
		return new ApiInfo(
				"Servicios API Ingeneo",
				"Descripción de API",
				"Versión 1.0",
				"",
				new Contact("Andres Piarpuzan", "", "andress0909@gmail.com"),
				"LICENSE",
				"LICENSE URL",
				Collections.emptyList()
				);
	}
	/**SWAGGER**/
	
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
	    CorsConfiguration configuration = new CorsConfiguration();
	    configuration.setAllowedOrigins(Collections.singletonList("*")); // <-- you may change "*"
	    configuration.setAllowedMethods(Arrays.asList("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH"));
	    configuration.setAllowCredentials(true);
	    configuration.setAllowedHeaders(Arrays.asList(
	            "Accept", "Origin", "Content-Type", "Depth", "User-Agent", "If-Modified-Since,",
	            "Cache-Control", "Authorization", "X-Req", "X-File-Size", "X-Requested-With", "X-File-Name"));
	    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    source.registerCorsConfiguration("/**", configuration);
	    return source;
	}
	
	@Bean
	public FilterRegistrationBean<CorsFilter> corsFilterRegistrationBean() {
	    FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(corsConfigurationSource()));
	    bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
	    return bean;
	}
	
}
