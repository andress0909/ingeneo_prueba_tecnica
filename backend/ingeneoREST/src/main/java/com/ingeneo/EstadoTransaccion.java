package com.ingeneo;

//Se crea una clase enum para controlar textos al momento de retornar mensajes en los errores
public enum EstadoTransaccion {
	//ESTADOS GENERALES
	APROBADA("01","APROBADA","Transacción aprobada y almacenada correctamente."),
    FALLIDA("02","FALLIDA","Transacción fallida y no almacenada."),
	//ESTADOS SOLICITUD TOKEN    
    USUARIO_NO_ENCONTRADO("03","USUARIO_NO_ENCONTRADO","Falta el campo usuario."),
    CONTRASENA_NO_ENCONTRADA("04","CONTRASENA_NO_ENCONTRADA","Falta el campo contrasena."),
    USUARIO_VACIO("05","USUARIO_VACIO","Los datos del usuario estan vacios."),
    CONTRASENA_VACIA("06","CONTRASENA_VACIA","El campo contrasena esta vacio."),
    USUARIO_NO_EXISTE("07","USUARIO_NO_EXISTE","Usuario no encontrado en el sistema."),
    CREDENCIALES_INVALIDAS("08","CREDENCIALES_INVALIDAS","Credenciales invalidas."),
    USUARIO_INACTIVO("09","USUARIO_INACTIVO","Usuario inactivo en el sistema."),
    USUARIO_SIN_PERFIL("10","USUARIO_SIN_PERFIL","Usuario no tiene perfiles asociados en el sistema."),
    
    EMPRESA_VACIA("11","EMPRESA_VACIA","Los datos de la empresa estan vacios."),
    EMPRESA_NO_EXISTE("11","EMPRESA_VACIA","Empresa no encontrada."),
    
    PRODUCTO_VACIO("12","PRODUCTO_VACIO","Los datos del producto estan vacios."),
    
    SOLICITUD_VACIA("13","SOLICITUD_VACIA","Los datos de la solicitud estan vacios."),
    
    TIPO_TRANSPORTE_NO_EXISTE("11","TIPO_TRANSPORTE_NO_EXISTE","Tipo Transporte no encontrado."),
    
    MEDIO_TRANSPORTE_NO_EXISTE("11","MEDIO_TRANSPORTE_NO_EXISTE","Medio Transporte no encontrado."),
	;
    
    private final String id; //Id del estado
    private final String estado;//descripcion del estado
    private final String descripcion;
    
	private EstadoTransaccion(String id, String estado, String descripcion) {
		this.id = id;
		this.estado = estado;
		this.descripcion = descripcion;
	}
	public String getId() {
		return id;
	}
	public String getEstado() {
		return estado;
	}
	public String getDescripcion() {
		return descripcion;
	}
}
