package com.ingeneo.exception;


import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ApiExceptionHandler {

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({ NotFoundException.class, org.springframework.web.client.HttpClientErrorException.NotFound.class,
			javassist.NotFoundException.class })
	@ResponseBody
	public ErrorInfo notFoundException(HttpServletRequest request, Exception e) {
		System.err.println("NotFound");
		return new ErrorInfo(e, request.getRequestURI());
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ BadRequestException.class, org.springframework.dao.DuplicateKeyException.class,
			org.springframework.web.HttpRequestMethodNotSupportedException.class,
			org.springframework.web.bind.MethodArgumentNotValidException.class,
			org.springframework.web.bind.MissingRequestHeaderException.class,
			org.springframework.web.bind.MissingServletRequestParameterException.class,
			org.springframework.web.method.annotation.MethodArgumentTypeMismatchException.class,
			org.springframework.http.converter.HttpMessageNotReadableException.class,
			IllegalArgumentException.class, 
			NullPointerException.class })
	@ResponseBody
	public ErrorInfo badRequestException(HttpServletRequest request, Exception e) {
		System.err.println("BadRequest");
		return new ErrorInfo(e, request.getRequestURI());
	}

	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ExceptionHandler({ ForbiddenException.class })
	@ResponseBody
	public ErrorInfo forBiddenException(HttpServletRequest request, Exception e) {
		System.err.println("ForBidden");
		return new ErrorInfo(e, request.getRequestURI());
	}

	@ResponseStatus(HttpStatus.CONFLICT)
	@ExceptionHandler({ ConflictException.class })
	@ResponseBody
	public ErrorInfo conflictException(HttpServletRequest request, Exception e) {
		System.err.println("Conflict");
		return new ErrorInfo(e, request.getRequestURI());
	}

	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ExceptionHandler({ UnAuthorizedException.class, org.springframework.security.access.AccessDeniedException.class })
	public void unauthorizedException() {
		System.err.println("UnAuthorized");
		// No soporte envio de body en el rest
	}

	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ExceptionHandler({ NoContentException.class })
	@ResponseBody
	public ErrorInfo noContentException(HttpServletRequest request, Exception e) {
		System.err.println("NoContent");
		return new ErrorInfo(e, request.getRequestURI());
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler({Exception.class})
	@ResponseBody
	public ErrorInfo fatalException(HttpServletRequest request,Exception e) {
		System.err.println("InternalServer");
		return new ErrorInfo(e,request.getRequestURI());
	}
}

//@Component public class ExtendedErrorAttributes extends DefaultErrorAttributes { @Override public Map getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) { final Map errorAttributes = super.getErrorAttributes(requestAttributes, includeStackTrace); final Throwable error = super.getError(requestAttributes); if (error instanceof TypeProvider) { final TypeProvider typeProvider = (TypeProvider) error; errorAttributes.put("type", typeProvider.getTypeIdentifier()); } return errorAttributes; } }
