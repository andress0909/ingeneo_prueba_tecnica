package com.ingeneo.exception;

public class ConflictException extends RuntimeException{
	private static final String DESCRIPTION = "Conflict Exception (409)";
	
	public ConflictException(String detalle) {
		super(DESCRIPTION+". "+detalle);
	}
}
