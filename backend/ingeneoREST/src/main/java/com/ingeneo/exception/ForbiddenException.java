package com.ingeneo.exception;

public class ForbiddenException extends RuntimeException{
	private static final String DESCRIPTION = "Forbidden Exception (403)";
	
	public ForbiddenException(String detalle) {
		super(DESCRIPTION+". "+detalle);
	}
}
