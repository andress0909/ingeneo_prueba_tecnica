package com.ingeneo.exception;

public class JWTException extends RuntimeException {
	private static final String DESCRIPTION = "JWT Exception (401)";
	
	public JWTException(String detalle) {
		super(DESCRIPTION+". "+detalle);
	}
}
