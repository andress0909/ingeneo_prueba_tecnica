package com.ingeneo.exception;

public class NoContentException extends RuntimeException {
	private static final String DESCRIPTION = "Not Content Exception (204)";
	
	public NoContentException(String detalle) {
		super(DESCRIPTION+". "+detalle);
	}
}
