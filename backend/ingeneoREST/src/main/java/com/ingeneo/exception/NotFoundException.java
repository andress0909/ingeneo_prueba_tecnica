package com.ingeneo.exception;

public class NotFoundException extends RuntimeException {
	private static final String DESCRIPTION = "Not Found Exception (404)";
	
	public NotFoundException(String detalle) {
		super(DESCRIPTION+". "+detalle);
	}
}
