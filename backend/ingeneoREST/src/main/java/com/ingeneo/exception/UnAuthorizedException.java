package com.ingeneo.exception;

public class UnAuthorizedException extends RuntimeException {
	private static final String DESCRIPTION = "Unathorized Exception (401)";
	
	public UnAuthorizedException(String detalle) {
		super(DESCRIPTION+". "+detalle);
	}
}
