/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Andres
 */
@Entity
@Table(name = "ciudad", catalog = "ingeneo", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Ciudad.findAll", query = "SELECT c FROM Ciudad c")
    , @NamedQuery(name = "Ciudad.findByIdCiu", query = "SELECT c FROM Ciudad c WHERE c.idCiu = :idCiu")
    , @NamedQuery(name = "Ciudad.findByNombre", query = "SELECT c FROM Ciudad c WHERE c.nombre = :nombre")
    , @NamedQuery(name = "Ciudad.findByDescripcion", query = "SELECT c FROM Ciudad c WHERE c.descripcion = :descripcion")})
public class Ciudad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_ciu")
    private Long idCiu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCiu", fetch = FetchType.LAZY)
    private List<LugarEntrega> lugarEntregaList;

    public Ciudad() {
    }

    public Ciudad(Long idCiu) {
        this.idCiu = idCiu;
    }

    public Ciudad(Long idCiu, String nombre, String descripcion) {
        this.idCiu = idCiu;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public Long getIdCiu() {
        return idCiu;
    }

    public void setIdCiu(Long idCiu) {
        this.idCiu = idCiu;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<LugarEntrega> getLugarEntregaList() {
        return lugarEntregaList;
    }

    public void setLugarEntregaList(List<LugarEntrega> lugarEntregaList) {
        this.lugarEntregaList = lugarEntregaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCiu != null ? idCiu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ciudad)) {
            return false;
        }
        Ciudad other = (Ciudad) object;
        if ((this.idCiu == null && other.idCiu != null) || (this.idCiu != null && !this.idCiu.equals(other.idCiu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ovflight.model.Ciudad[ idCiu=" + idCiu + " ]";
    }
    
}
