/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Andres
 */
@Entity
@Table(name = "flota", catalog = "ingeneo", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Flota.findAll", query = "SELECT f FROM Flota f")
    , @NamedQuery(name = "Flota.findByIdMtr", query = "SELECT f FROM Flota f WHERE f.idMtr = :idMtr")
    , @NamedQuery(name = "Flota.findByNumeroFlota", query = "SELECT f FROM Flota f WHERE f.numeroFlota = :numeroFlota")})
public class Flota implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_mtr")
    private Long idMtr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "numero_flota")
    private String numeroFlota;
    @JoinColumn(name = "id_mtr", referencedColumnName = "id_mtr", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private MedioTransporte medioTransporte;

    public Flota() {
    }

    public Flota(Long idMtr) {
        this.idMtr = idMtr;
    }

    public Flota(Long idMtr, String numeroFlota) {
        this.idMtr = idMtr;
        this.numeroFlota = numeroFlota;
    }

    public Long getIdMtr() {
        return idMtr;
    }

    public void setIdMtr(Long idMtr) {
        this.idMtr = idMtr;
    }

    public String getNumeroFlota() {
        return numeroFlota;
    }

    public void setNumeroFlota(String numeroFlota) {
        this.numeroFlota = numeroFlota;
    }

    public MedioTransporte getMedioTransporte() {
        return medioTransporte;
    }

    public void setMedioTransporte(MedioTransporte medioTransporte) {
        this.medioTransporte = medioTransporte;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMtr != null ? idMtr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Flota)) {
            return false;
        }
        Flota other = (Flota) object;
        if ((this.idMtr == null && other.idMtr != null) || (this.idMtr != null && !this.idMtr.equals(other.idMtr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ovflight.model.Flota[ idMtr=" + idMtr + " ]";
    }
    
}
