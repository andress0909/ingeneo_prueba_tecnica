/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Andres
 */
@Entity
@Table(name = "lugar_entrega", catalog = "ingeneo", schema = "public")
@NamedQueries({
    @NamedQuery(name = "LugarEntrega.findAll", query = "SELECT l FROM LugarEntrega l")
    , @NamedQuery(name = "LugarEntrega.findByIdLue", query = "SELECT l FROM LugarEntrega l WHERE l.idLue = :idLue")
    , @NamedQuery(name = "LugarEntrega.findByNombre", query = "SELECT l FROM LugarEntrega l WHERE l.nombre = :nombre")
    , @NamedQuery(name = "LugarEntrega.findByDireccion", query = "SELECT l FROM LugarEntrega l WHERE l.direccion = :direccion")})
public class LugarEntrega implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_lue")
    private Long idLue;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "direccion")
    private String direccion;
    @OneToMany(mappedBy = "idLue", fetch = FetchType.LAZY)
    private List<LogisticaEnvio> logisticaEnvioList;
    @JoinColumn(name = "id_ciu", referencedColumnName = "id_ciu")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Ciudad idCiu;
    @JoinColumn(name = "id_tlu", referencedColumnName = "id_tlu")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoLugar idTlu;
    @JoinColumn(name = "id_ttr", referencedColumnName = "id_ttr")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoTransporte idTtr;

    public LugarEntrega() {
    }

    public LugarEntrega(Long idLue) {
        this.idLue = idLue;
    }

    public LugarEntrega(Long idLue, String nombre, String direccion) {
        this.idLue = idLue;
        this.nombre = nombre;
        this.direccion = direccion;
    }

    public Long getIdLue() {
        return idLue;
    }

    public void setIdLue(Long idLue) {
        this.idLue = idLue;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<LogisticaEnvio> getLogisticaEnvioList() {
        return logisticaEnvioList;
    }

    public void setLogisticaEnvioList(List<LogisticaEnvio> logisticaEnvioList) {
        this.logisticaEnvioList = logisticaEnvioList;
    }

    public Ciudad getIdCiu() {
        return idCiu;
    }

    public void setIdCiu(Ciudad idCiu) {
        this.idCiu = idCiu;
    }

    public TipoLugar getIdTlu() {
        return idTlu;
    }

    public void setIdTlu(TipoLugar idTlu) {
        this.idTlu = idTlu;
    }

    public TipoTransporte getIdTtr() {
        return idTtr;
    }

    public void setIdTtr(TipoTransporte idTtr) {
        this.idTtr = idTtr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLue != null ? idLue.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LugarEntrega)) {
            return false;
        }
        LugarEntrega other = (LugarEntrega) object;
        if ((this.idLue == null && other.idLue != null) || (this.idLue != null && !this.idLue.equals(other.idLue))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ovflight.model.LugarEntrega[ idLue=" + idLue + " ]";
    }
    
}
