/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Andres
 */
@Entity
@Table(name = "medio_transporte", catalog = "ingeneo", schema = "public")
@NamedQueries({
    @NamedQuery(name = "MedioTransporte.findAll", query = "SELECT m FROM MedioTransporte m")
    , @NamedQuery(name = "MedioTransporte.findByIdMtr", query = "SELECT m FROM MedioTransporte m WHERE m.idMtr = :idMtr")
    , @NamedQuery(name = "MedioTransporte.findByDescripcion", query = "SELECT m FROM MedioTransporte m WHERE m.descripcion = :descripcion")})
public class MedioTransporte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_mtr")
    private Long idMtr;
    @Size(max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMtr", fetch = FetchType.LAZY)
    private List<LogisticaEnvio> logisticaEnvioList;
    @JoinColumn(name = "id_ttr", referencedColumnName = "id_ttr")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoTransporte idTtr;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "medioTransporte", fetch = FetchType.LAZY)
    private Flota flota;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "medioTransporte", fetch = FetchType.LAZY)
    private Vehiculo vehiculo;

    public MedioTransporte() {
    }

    public MedioTransporte(Long idMtr) {
        this.idMtr = idMtr;
    }

    public Long getIdMtr() {
        return idMtr;
    }

    public void setIdMtr(Long idMtr) {
        this.idMtr = idMtr;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<LogisticaEnvio> getLogisticaEnvioList() {
        return logisticaEnvioList;
    }

    public void setLogisticaEnvioList(List<LogisticaEnvio> logisticaEnvioList) {
        this.logisticaEnvioList = logisticaEnvioList;
    }

    public TipoTransporte getIdTtr() {
        return idTtr;
    }

    public void setIdTtr(TipoTransporte idTtr) {
        this.idTtr = idTtr;
    }

    public Flota getFlota() {
        return flota;
    }

    public void setFlota(Flota flota) {
        this.flota = flota;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMtr != null ? idMtr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedioTransporte)) {
            return false;
        }
        MedioTransporte other = (MedioTransporte) object;
        if ((this.idMtr == null && other.idMtr != null) || (this.idMtr != null && !this.idMtr.equals(other.idMtr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ovflight.model.MedioTransporte[ idMtr=" + idMtr + " ]";
    }
    
}
