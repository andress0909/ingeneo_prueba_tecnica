/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Andres
 */
@Entity
@Table(name = "solicitud_envio", catalog = "ingeneo", schema = "public")
@NamedQueries({
    @NamedQuery(name = "SolicitudEnvio.findAll", query = "SELECT s FROM SolicitudEnvio s")
    , @NamedQuery(name = "SolicitudEnvio.findByIdSol", query = "SELECT s FROM SolicitudEnvio s WHERE s.idSol = :idSol")
    , @NamedQuery(name = "SolicitudEnvio.findByFechaSolicitud", query = "SELECT s FROM SolicitudEnvio s WHERE s.fechaSolicitud = :fechaSolicitud")
    , @NamedQuery(name = "SolicitudEnvio.findByObservaciones", query = "SELECT s FROM SolicitudEnvio s WHERE s.observaciones = :observaciones")})
public class SolicitudEnvio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_sol")
    @GeneratedValue(generator="seqsolenv") 
    @SequenceGenerator(name="seqsolenv",sequenceName="sec_solenv", allocationSize=1)
    private Long idSol;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_solicitud")
    @Temporal(TemporalType.DATE)
    private Date fechaSolicitud;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "observaciones")
    private String observaciones;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSol", fetch = FetchType.LAZY)
    private List<LogisticaEnvio> logisticaEnvioList;
    @JoinColumn(name = "id_emp", referencedColumnName = "id_emp")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Empresa idEmp;
    @JoinColumn(name = "id_ttr", referencedColumnName = "id_ttr")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoTransporte idTtr;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "solicitudEnvio", fetch = FetchType.LAZY)
    private List<SolicitudProducto> solicitudProductoList;

    public SolicitudEnvio() {
    }

    public SolicitudEnvio(Long idSol) {
        this.idSol = idSol;
    }

    public SolicitudEnvio(Long idSol, Date fechaSolicitud, String observaciones) {
        this.idSol = idSol;
        this.fechaSolicitud = fechaSolicitud;
        this.observaciones = observaciones;
    }

    public Long getIdSol() {
        return idSol;
    }

    public void setIdSol(Long idSol) {
        this.idSol = idSol;
    }

    public Date getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(Date fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public List<LogisticaEnvio> getLogisticaEnvioList() {
        return logisticaEnvioList;
    }

    public void setLogisticaEnvioList(List<LogisticaEnvio> logisticaEnvioList) {
        this.logisticaEnvioList = logisticaEnvioList;
    }

    public Empresa getIdEmp() {
        return idEmp;
    }

    public void setIdEmp(Empresa idEmp) {
        this.idEmp = idEmp;
    }

    public TipoTransporte getIdTtr() {
        return idTtr;
    }

    public void setIdTtr(TipoTransporte idTtr) {
        this.idTtr = idTtr;
    }

    public List<SolicitudProducto> getSolicitudProductoList() {
        return solicitudProductoList;
    }

    public void setSolicitudProductoList(List<SolicitudProducto> solicitudProductoList) {
        this.solicitudProductoList = solicitudProductoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSol != null ? idSol.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudEnvio)) {
            return false;
        }
        SolicitudEnvio other = (SolicitudEnvio) object;
        if ((this.idSol == null && other.idSol != null) || (this.idSol != null && !this.idSol.equals(other.idSol))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ovflight.model.SolicitudEnvio[ idSol=" + idSol + " ]";
    }
    
}
