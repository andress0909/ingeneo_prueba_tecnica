/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Andres
 */
@Entity
@Table(name = "solicitud_producto", catalog = "ingeneo", schema = "public")
@NamedQueries({
    @NamedQuery(name = "SolicitudProducto.findAll", query = "SELECT s FROM SolicitudProducto s")
    , @NamedQuery(name = "SolicitudProducto.findByIdPro", query = "SELECT s FROM SolicitudProducto s WHERE s.solicitudProductoPK.idPro = :idPro")
    , @NamedQuery(name = "SolicitudProducto.findByIdSol", query = "SELECT s FROM SolicitudProducto s WHERE s.solicitudProductoPK.idSol = :idSol")
    , @NamedQuery(name = "SolicitudProducto.findByCantidad", query = "SELECT s FROM SolicitudProducto s WHERE s.cantidad = :cantidad")
    , @NamedQuery(name = "SolicitudProducto.findByPrecio", query = "SELECT s FROM SolicitudProducto s WHERE s.precio = :precio")
    , @NamedQuery(name = "SolicitudProducto.findByPrecioDescuento", query = "SELECT s FROM SolicitudProducto s WHERE s.precioDescuento = :precioDescuento")
    , @NamedQuery(name = "SolicitudProducto.findByPorcentajeDescuento", query = "SELECT s FROM SolicitudProducto s WHERE s.porcentajeDescuento = :porcentajeDescuento")})
public class SolicitudProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SolicitudProductoPK solicitudProductoPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private long cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio")
    private double precio;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "precio_descuento")
    private Double precioDescuento;
    @Column(name = "porcentaje_descuento")
    private Double porcentajeDescuento;
    @JoinColumn(name = "id_sol", referencedColumnName = "id_sol", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private SolicitudEnvio solicitudEnvio;
    @JoinColumn(name = "id_pro", referencedColumnName = "id_pro", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TipoProducto tipoProducto;

    public SolicitudProducto() {
    }

    public SolicitudProducto(SolicitudProductoPK solicitudProductoPK) {
        this.solicitudProductoPK = solicitudProductoPK;
    }

    public SolicitudProducto(SolicitudProductoPK solicitudProductoPK, long cantidad, double precio) {
        this.solicitudProductoPK = solicitudProductoPK;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public SolicitudProducto(long idPro, long idSol) {
        this.solicitudProductoPK = new SolicitudProductoPK(idPro, idSol);
    }

    public SolicitudProductoPK getSolicitudProductoPK() {
        return solicitudProductoPK;
    }

    public void setSolicitudProductoPK(SolicitudProductoPK solicitudProductoPK) {
        this.solicitudProductoPK = solicitudProductoPK;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Double getPrecioDescuento() {
        return precioDescuento;
    }

    public void setPrecioDescuento(Double precioDescuento) {
        this.precioDescuento = precioDescuento;
    }

    public Double getPorcentajeDescuento() {
        return porcentajeDescuento;
    }

    public void setPorcentajeDescuento(Double porcentajeDescuento) {
        this.porcentajeDescuento = porcentajeDescuento;
    }

    public SolicitudEnvio getSolicitudEnvio() {
        return solicitudEnvio;
    }

    public void setSolicitudEnvio(SolicitudEnvio solicitudEnvio) {
        this.solicitudEnvio = solicitudEnvio;
    }

    public TipoProducto getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(TipoProducto tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (solicitudProductoPK != null ? solicitudProductoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudProducto)) {
            return false;
        }
        SolicitudProducto other = (SolicitudProducto) object;
        if ((this.solicitudProductoPK == null && other.solicitudProductoPK != null) || (this.solicitudProductoPK != null && !this.solicitudProductoPK.equals(other.solicitudProductoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ovflight.model.SolicitudProducto[ solicitudProductoPK=" + solicitudProductoPK + " ]";
    }
    
}
