/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Andres
 */
@Embeddable
public class SolicitudProductoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_pro")
    private long idPro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_sol")
    private long idSol;

    public SolicitudProductoPK() {
    }

    public SolicitudProductoPK(long idPro, long idSol) {
        this.idPro = idPro;
        this.idSol = idSol;
    }

    public long getIdPro() {
        return idPro;
    }

    public void setIdPro(long idPro) {
        this.idPro = idPro;
    }

    public long getIdSol() {
        return idSol;
    }

    public void setIdSol(long idSol) {
        this.idSol = idSol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idPro;
        hash += (int) idSol;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudProductoPK)) {
            return false;
        }
        SolicitudProductoPK other = (SolicitudProductoPK) object;
        if (this.idPro != other.idPro) {
            return false;
        }
        if (this.idSol != other.idSol) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ovflight.model.SolicitudProductoPK[ idPro=" + idPro + ", idSol=" + idSol + " ]";
    }
    
}
