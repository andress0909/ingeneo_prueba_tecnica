/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Andres
 */
@Entity
@Table(name = "tipo_lugar", catalog = "ingeneo", schema = "public")
@NamedQueries({
    @NamedQuery(name = "TipoLugar.findAll", query = "SELECT t FROM TipoLugar t")
    , @NamedQuery(name = "TipoLugar.findByIdTlu", query = "SELECT t FROM TipoLugar t WHERE t.idTlu = :idTlu")
    , @NamedQuery(name = "TipoLugar.findByNombre", query = "SELECT t FROM TipoLugar t WHERE t.nombre = :nombre")
    , @NamedQuery(name = "TipoLugar.findByDescripcion", query = "SELECT t FROM TipoLugar t WHERE t.descripcion = :descripcion")})
public class TipoLugar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_tlu")
    private Long idTlu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTlu", fetch = FetchType.LAZY)
    private List<LugarEntrega> lugarEntregaList;

    public TipoLugar() {
    }

    public TipoLugar(Long idTlu) {
        this.idTlu = idTlu;
    }

    public TipoLugar(Long idTlu, String nombre, String descripcion) {
        this.idTlu = idTlu;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public Long getIdTlu() {
        return idTlu;
    }

    public void setIdTlu(Long idTlu) {
        this.idTlu = idTlu;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<LugarEntrega> getLugarEntregaList() {
        return lugarEntregaList;
    }

    public void setLugarEntregaList(List<LugarEntrega> lugarEntregaList) {
        this.lugarEntregaList = lugarEntregaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTlu != null ? idTlu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoLugar)) {
            return false;
        }
        TipoLugar other = (TipoLugar) object;
        if ((this.idTlu == null && other.idTlu != null) || (this.idTlu != null && !this.idTlu.equals(other.idTlu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ovflight.model.TipoLugar[ idTlu=" + idTlu + " ]";
    }
    
}
