/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ingeneo.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Andres
 */
@Entity
@Table(name = "tipo_transporte", catalog = "ingeneo", schema = "public")
@NamedQueries({
    @NamedQuery(name = "TipoTransporte.findAll", query = "SELECT t FROM TipoTransporte t")
    , @NamedQuery(name = "TipoTransporte.findByIdTtr", query = "SELECT t FROM TipoTransporte t WHERE t.idTtr = :idTtr")
    , @NamedQuery(name = "TipoTransporte.findByNombre", query = "SELECT t FROM TipoTransporte t WHERE t.nombre = :nombre")
    , @NamedQuery(name = "TipoTransporte.findByDescripcion", query = "SELECT t FROM TipoTransporte t WHERE t.descripcion = :descripcion")})
public class TipoTransporte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_ttr")
    private Long idTtr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTtr", fetch = FetchType.LAZY)
    private List<MedioTransporte> medioTransporteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTtr", fetch = FetchType.LAZY)
    private List<SolicitudEnvio> solicitudEnvioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTtr", fetch = FetchType.LAZY)
    private List<LugarEntrega> lugarEntregaList;

    public TipoTransporte() {
    }

    public TipoTransporte(Long idTtr) {
        this.idTtr = idTtr;
    }

    public TipoTransporte(Long idTtr, String nombre, String descripcion) {
        this.idTtr = idTtr;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public Long getIdTtr() {
        return idTtr;
    }

    public void setIdTtr(Long idTtr) {
        this.idTtr = idTtr;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<MedioTransporte> getMedioTransporteList() {
        return medioTransporteList;
    }

    public void setMedioTransporteList(List<MedioTransporte> medioTransporteList) {
        this.medioTransporteList = medioTransporteList;
    }

    public List<SolicitudEnvio> getSolicitudEnvioList() {
        return solicitudEnvioList;
    }

    public void setSolicitudEnvioList(List<SolicitudEnvio> solicitudEnvioList) {
        this.solicitudEnvioList = solicitudEnvioList;
    }

    public List<LugarEntrega> getLugarEntregaList() {
        return lugarEntregaList;
    }

    public void setLugarEntregaList(List<LugarEntrega> lugarEntregaList) {
        this.lugarEntregaList = lugarEntregaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTtr != null ? idTtr.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoTransporte)) {
            return false;
        }
        TipoTransporte other = (TipoTransporte) object;
        if ((this.idTtr == null && other.idTtr != null) || (this.idTtr != null && !this.idTtr.equals(other.idTtr))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ovflight.model.TipoTransporte[ idTtr=" + idTtr + " ]";
    }
    
}
