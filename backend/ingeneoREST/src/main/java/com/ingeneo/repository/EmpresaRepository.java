package com.ingeneo.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.ingeneo.model.Empresa;

public interface EmpresaRepository extends JpaRepository<Empresa, Long>  {

}
