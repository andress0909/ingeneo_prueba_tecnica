package com.ingeneo.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ingeneo.model.LogisticaEnvio;

public interface LogisticaEnvioRepository extends JpaRepository<LogisticaEnvio, Long>  {

}
