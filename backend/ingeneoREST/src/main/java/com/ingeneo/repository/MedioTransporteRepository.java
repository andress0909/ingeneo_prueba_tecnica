package com.ingeneo.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ingeneo.model.MedioTransporte;

public interface MedioTransporteRepository extends JpaRepository<MedioTransporte, Long>  {

}
