package com.ingeneo.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ingeneo.model.SolicitudEnvio;

public interface SolicitudEnvioRepository extends JpaRepository<SolicitudEnvio, Long>  {

}
