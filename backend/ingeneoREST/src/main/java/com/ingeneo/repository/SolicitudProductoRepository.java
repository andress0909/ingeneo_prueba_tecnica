package com.ingeneo.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ingeneo.model.SolicitudEnvio;
import com.ingeneo.model.SolicitudProducto;

public interface SolicitudProductoRepository extends JpaRepository<SolicitudProducto, Long>  {

}
