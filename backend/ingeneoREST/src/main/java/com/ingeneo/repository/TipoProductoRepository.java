package com.ingeneo.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.ingeneo.model.TipoProducto;

public interface TipoProductoRepository extends JpaRepository<TipoProducto, Long>  {

}
