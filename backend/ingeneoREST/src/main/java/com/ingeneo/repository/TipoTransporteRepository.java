package com.ingeneo.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ingeneo.model.TipoTransporte;

public interface TipoTransporteRepository extends JpaRepository<TipoTransporte, Long>  {

}
