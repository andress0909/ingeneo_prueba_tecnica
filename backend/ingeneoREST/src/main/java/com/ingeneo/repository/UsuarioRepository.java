package com.ingeneo.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.ingeneo.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>  {
	Usuario findByUsuario(String usuario);
}
