package com.ingeneo.request;


public class SolicitudProductoRequest {

	private Long idPro;
	private Long cantidad;
	public Long getIdPro() {
		return idPro;
	}
	public void setIdPro(Long idPro) {
		this.idPro = idPro;
	}
	public Long getCantidad() {
		return cantidad;
	}
	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

}
