package com.ingeneo.request;

import java.util.Date;
import java.util.List;

public class SolicitudRequest {

	// private Long idSol;
	private Date fechaSolicitud;
	private String observaciones;
	private Long idEmp;
	private Long idTtr;
	private Long idMtr;
	private Long idLue;
	private List<SolicitudProductoRequest> productos;

	public Long getIdLue() {
		return idLue;
	}

	public void setIdLue(Long idLue) {
		this.idLue = idLue;
	}

	public Long getIdMtr() {
		return idMtr;
	}

	public void setIdMtr(Long idMtr) {
		this.idMtr = idMtr;
	}

	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Long getIdEmp() {
		return idEmp;
	}

	public void setIdEmp(Long idEmp) {
		this.idEmp = idEmp;
	}

	public Long getIdTtr() {
		return idTtr;
	}

	public void setIdTtr(Long idTtr) {
		this.idTtr = idTtr;
	}

	public List<SolicitudProductoRequest> getProductos() {
		return productos;
	}

	public void setProductos(List<SolicitudProductoRequest> productos) {
		this.productos = productos;
	}

	public static boolean isNull(SolicitudRequest us) {
		if (us.getFechaSolicitud() != null && us.getObservaciones() != null && us.getIdEmp() != null && us.getIdTtr() != null && us.getProductos()!=null) {
			return false;
		}
		if (!(us.getFechaSolicitud().equals("") || us.getObservaciones().equals("") || us.getIdEmp().equals("")
				|| us.getIdTtr().equals("") || us.getProductos().isEmpty())) {
			return false;
		}
		return true;
	}

}
