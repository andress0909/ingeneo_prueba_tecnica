package com.ingeneo.request;

public class UsuarioRequest {

	// private Long idUsu;
	private String identificacion;
	private String nombres;
	private String apellidos;
	private String correo;
	private String celular;
	private String usuario;
	private String contrasena;

	/*
	 * public Long getIdUsu() { return idUsu; } public void setIdUsu(Long idUsu) {
	 * this.idUsu = idUsu; }
	 */
	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public static boolean isNull(UsuarioRequest us) {
		if (us.getIdentificacion() != null && us.getNombres() != null && us.getApellidos() != null
				&& us.getCorreo() != null && us.getCelular() != null && us.getUsuario() != null
				&& us.getContrasena() != null) {
			return false;
		}
		if (!(us.getIdentificacion().equals("") || us.getNombres().equals("") || us.getApellidos().equals("")
				|| us.getCorreo().equals("") || us.getCelular().equals("") || us.getUsuario().equals("")
				|| us.getContrasena().equals(""))) {
			return false;
		}
		return true;
	}

	public static boolean isEmpty(UsuarioRequest us) {
		return true;
	}
}
