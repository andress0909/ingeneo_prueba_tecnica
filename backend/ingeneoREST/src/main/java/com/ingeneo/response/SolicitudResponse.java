package com.ingeneo.response;

import java.util.Date;
import java.util.List;

public class SolicitudResponse {

	private Long idSol;
	private Date fechaSolicitud;
	private String observaciones;
	private String empresa;
	private String tipo_transporte;
	private String medio_transporte;
	private Date fecha_entrega;
	private String numero_guia;
	private Double precio;
	private List<TipoProductoResponse> productos;
	
	public Long getIdSol() {
		return idSol;
	}
	public void setIdSol(Long idSol) {
		this.idSol = idSol;
	}
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}
	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getTipo_transporte() {
		return tipo_transporte;
	}
	public void setTipo_transporte(String tipo_transporte) {
		this.tipo_transporte = tipo_transporte;
	}
	public String getMedio_transporte() {
		return medio_transporte;
	}
	public void setMedio_transporte(String medio_transporte) {
		this.medio_transporte = medio_transporte;
	}
	public Date getFecha_entrega() {
		return fecha_entrega;
	}
	public void setFecha_entrega(Date fecha_entrega) {
		this.fecha_entrega = fecha_entrega;
	}
	public String getNumero_guia() {
		return numero_guia;
	}
	public void setNumero_guia(String numero_guia) {
		this.numero_guia = numero_guia;
	}
	public Double getPrecio() {
		return precio;
	}
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	public List<TipoProductoResponse> getProductos() {
		return productos;
	}
	public void setProductos(List<TipoProductoResponse> productos) {
		this.productos = productos;
	}

}
