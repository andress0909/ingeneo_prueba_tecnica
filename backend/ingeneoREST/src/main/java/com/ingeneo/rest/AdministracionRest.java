package com.ingeneo.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ingeneo.request.EmpresaRequest;
import com.ingeneo.request.TipoProductoRequest;
import com.ingeneo.request.UsuarioRequest;
import com.ingeneo.response.EmpresaResponse;
import com.ingeneo.response.TipoProductoResponse;
import com.ingeneo.response.UsuarioResponse;
import com.ingeneo.service.EmpresaService;
import com.ingeneo.service.TipoProductoService;
import com.ingeneo.service.UsuarioService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/admin")
public class AdministracionRest {

	@Autowired
	UsuarioService usuarioService;
	@Autowired
	EmpresaService empresaService;
	@Autowired
	TipoProductoService tipoproductoService;
	
	@ApiOperation(value = "Crear Usuarios para el portal"
            ,notes = "Aquí se crean los diferentes usuarios que interactuaran en el sistema.")
	@PostMapping(value = "/creausuario")
	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	public ResponseEntity<List<UsuarioResponse>> crearusuario(@RequestBody UsuarioRequest usuarioRqs) {
		return usuarioService.crear(usuarioRqs);
	}
	
	@ApiOperation(value = "Crear Empresas - Clientes para el portal"
            ,notes = "Aquí se crean las diferentes empresas que realizarán solicitudes de envio (Logística Marítima y Terrestre).")
	@PostMapping(value = "/creaempresa")
	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	public ResponseEntity<List<EmpresaResponse>> creaempresa(@RequestBody EmpresaRequest empresaRqs) {
		return empresaService.crear(empresaRqs);
	}
	
	@ApiOperation(value = "Crear Productos - Productos ofertados en el portal"
            ,notes = "Aquí se crean los diferentes productos que estarán disponibles para los clientes sean para Logística Marítima y Terrestre).")
	@PostMapping(value = "/creaproducto")
	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	public ResponseEntity<List<TipoProductoResponse>> creaproducto(@RequestBody TipoProductoRequest productoRqs) {
		return tipoproductoService.crear(productoRqs);
	}

}
