package com.ingeneo.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ingeneo.request.LoginRequest;
import com.ingeneo.response.LoginResponse;
import com.ingeneo.service.LoginRestService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/login")
public class LoginRest {

	@Autowired
	LoginRestService loginRestService;
		
	@ApiOperation(value = "Conseguir Token"
            ,notes = "Aquí se solicita la creación de un token mediante un usuario y contraseña para un posterior consumo de los demas Servicios Rest del sistema.")
	@PostMapping(value = "/getToken")
	public ResponseEntity<LoginResponse> getToken(@RequestBody LoginRequest loginRqs) {
		return loginRestService.getToken(loginRqs);
	}

}
