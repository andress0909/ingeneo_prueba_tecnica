package com.ingeneo.rest;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ingeneo.request.SolicitudRequest;
import com.ingeneo.request.UsuarioRequest;
import com.ingeneo.response.SolicitudResponse;
import com.ingeneo.response.UsuarioResponse;
import com.ingeneo.service.LogisticaService;

import io.swagger.annotations.ApiOperation;



@RestController
@RequestMapping("/logistica")
public class LogisticaRest {

	@Autowired
	LogisticaService logisticaService;
	
	@ApiOperation(value = "Crear Solicitud - Solicitudes a realizar la Logistica"
            ,notes = "Aquí se crean las solicitudes de envio para Logistica Maritima o Terrestre.")
	@PostMapping(value = "/creasolicitud")
	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	public ResponseEntity<List<SolicitudResponse>> crearsolicitud(@RequestBody SolicitudRequest solicitudRqs) throws ParseException {
		return logisticaService.crear(solicitudRqs);
	}
	
	@ApiOperation(value = "Consulta Solicitud - Consulta todas las solicitudes de Logistica"
            ,notes = "Aquí se crean las solicitudes de envio para Logistica Maritima o Terrestre.")
	@GetMapping(value = "/consultasolicitud")
	@PreAuthorize("hasAuthority('ADMINISTRADOR')")
	public ResponseEntity<List<SolicitudResponse>> consultasolicitud() throws ParseException {
		return logisticaService.consultasolicitudes();
	}
	

}
