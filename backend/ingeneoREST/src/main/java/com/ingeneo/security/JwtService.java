package com.ingeneo.security;



import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.ingeneo.exception.JWTException;

import sun.awt.AWTAccessor.SystemColorAccessor;

//Servicio que generará el Token de acuerdo a parametros asignados, se combina roles, clave secreta, usuario
//La autenticación sera de tipo Bearer
//Tambien se realizará una decodificacion del token
@Service
public class JwtService {
	
	public static final String BEARER = "Bearer ";
	
	public static final String USER = "user";
	public static final String ROLES = "roles";
	public static final String ISSUER = "Ingeneo";
	public static final int EXPIRES_IN_MILLISECOND = 86400000;
	public static final String SECRET = "Ingeneo*%2023";
	
	public String createToken(String user , List<String> roles) {
		System.out.println("Genrando Token...");
		return JWT.create()
				.withIssuer(ISSUER)
				.withIssuedAt(new Date())
				.withNotBefore(new Date())
				.withExpiresAt(new Date(System.currentTimeMillis()+EXPIRES_IN_MILLISECOND))
				.withClaim(USER, user)
				.withArrayClaim(ROLES, roles.toArray(new String[0]))
				.sign(Algorithm.HMAC256(SECRET));
	}
	
	public boolean isBearer (String authorization) {
		System.out.println("authorization..."+authorization);
		return authorization != null && authorization.startsWith(BEARER) && authorization.split("\\.").length == 3;
	}
	
	public String user (String authorization) {
		System.out.println("user: "+this.verify(authorization).getClaim(USER).asString());
		return this.verify(authorization).getClaim(USER).asString();
	}
	
	private DecodedJWT verify(String authorization) throws JWTException{
		if(!this.isBearer(authorization)) {
			System.out.println("No es Bearer");
			throw new JWTException("No es Bearer"); 
		}
		try {
			return JWT.require(Algorithm.HMAC256(SECRET))
					.withIssuer(ISSUER).build()
					.verify(authorization.substring(BEARER.length()));
			
		}catch(Exception exception) {
			throw new JWTException("Hubo un problema JWT "+exception.getMessage()); 
		}
	}
	
	public List<String> roles(String authorization)throws JWTException{
System.out.println("Roles: "+Arrays.asList(this.verify(authorization).getClaim(ROLES).asArray(String.class)).get(0));
		return Arrays.asList(this.verify(authorization).getClaim(ROLES).asArray(String.class));
	}

}
