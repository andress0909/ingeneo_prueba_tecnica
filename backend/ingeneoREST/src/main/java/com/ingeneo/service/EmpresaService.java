package com.ingeneo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ingeneo.EstadoTransaccion;
import com.ingeneo.exception.BadRequestException;
import com.ingeneo.model.Empresa;
import com.ingeneo.repository.EmpresaRepository;
import com.ingeneo.request.EmpresaRequest;
import com.ingeneo.response.EmpresaResponse;

@Service
public class EmpresaService {

	@Autowired
	EmpresaRepository empresaRepository;

	// Metodo que inserta los datos de la empresa en la BD
	public ResponseEntity<List<EmpresaResponse>> crear(EmpresaRequest empresaRqs) {
		if (!EmpresaRequest.isNull(empresaRqs)) {
			EmpresaResponse empresaRes = new EmpresaResponse();
			// Asignan datos al model para realizar la insercion en la BD
			Empresa empresa = new Empresa();
			empresa.setNit(empresaRqs.getNit());
			empresa.setNombre(empresaRqs.getNombre());
			empresa.setCorreo(empresaRqs.getCorreo());
			empresa.setDireccion(empresaRqs.getDireccion());
			empresa.setTelefono(empresaRqs.getTelefono());

			empresaRepository.save(empresa);

			empresaRes.setIdEmp(empresa.getIdEmp());
			empresaRes.setNit(empresa.getNit());
			empresaRes.setNombre(empresa.getNombre());
			empresaRes.setCorreo(empresa.getCorreo());
			empresaRes.setDireccion(empresa.getDireccion());
			empresaRes.setTelefono(empresa.getTelefono());

			List<EmpresaResponse> list = new ArrayList<EmpresaResponse>();
			list.add(empresaRes);
			return ResponseEntity.ok(list);
		} else {
			throw new BadRequestException(EstadoTransaccion.EMPRESA_VACIA.getDescripcion());
		}
	}

}
