package com.ingeneo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ingeneo.EstadoTransaccion;
import com.ingeneo.exception.BadRequestException;
import com.ingeneo.model.Empresa;
import com.ingeneo.model.TipoProducto;
import com.ingeneo.repository.TipoProductoRepository;
import com.ingeneo.request.EmpresaRequest;
import com.ingeneo.request.TipoProductoRequest;
import com.ingeneo.response.EmpresaResponse;
import com.ingeneo.response.TipoProductoResponse;

@Service
public class TipoProductoService {

	@Autowired
	TipoProductoRepository productoRepository;

	// Metodo que inserta los datos de la empresa en la BD
	public ResponseEntity<List<TipoProductoResponse>> crear(TipoProductoRequest productoRqs) {
		if (!TipoProductoRequest.isNull(productoRqs)) {
			TipoProductoResponse productoRes = new TipoProductoResponse();
			// Asignan datos al model para realizar la insercion en la BD
			TipoProducto producto = new TipoProducto();
			producto.setNombre(productoRqs.getNombre());
			producto.setDescripcion(productoRqs.getDescripcion());
			producto.setPrecioEnvio(productoRqs.getPrecioEnvio());

			productoRepository.save(producto);

			productoRes.setIdPro(producto.getIdPro());
			productoRes.setNombre(producto.getNombre());
			productoRes.setDescripcion(producto.getDescripcion());
			productoRes.setPrecioEnvio(producto.getPrecioEnvio());

			List<TipoProductoResponse> list = new ArrayList<TipoProductoResponse>();
			list.add(productoRes);
			return ResponseEntity.ok(list);
		} else {
			throw new BadRequestException(EstadoTransaccion.PRODUCTO_VACIO.getDescripcion());
		}
	}

}
