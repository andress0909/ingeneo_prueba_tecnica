import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpEventType,
  HttpErrorResponse,
} from '@angular/common/http';
import { Router } from '@angular/router';
import { Usuario } from './models/usuario';
import { Empresa } from './models/empresa';
import { Producto } from './models/producto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiservicesService {

  private path = "https://ingeneorest.sofgold.net/";
  //private path = "http://localhost:8080/";

  private urlCreaempresa = this.path + 'admin/creaempresa/';
  private urlCreausuario = this.path + 'admin/creausuario/';
  private urlCreaproducto = this.path + 'admin/creaproducto/';

  headers = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYmYiOjE2ODQ2NDg2NDAsInJvbGVzIjpbIkFETUlOSVNUUkFET1IiXSwiaXNzIjoiSW5nZW5lbyIsImV4cCI6MTY4NDczNTA0MCwiaWF0IjoxNjg0NjQ4NjQwLCJ1c2VyIjoiaW5nZW5lbyJ9.T8MM47L5NykE6V8p18ft23lhUzsPsiY0yeEPEWA_dV8');

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }


  public postCreaUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.urlCreausuario, usuario, {
      headers: this.headers,
    });
  }

  public postCreaEmpresa(empresa: Empresa): Observable<Empresa> {
    return this.http.post<Empresa>(this.urlCreaempresa, empresa, {
      headers: this.headers,
    });
  }

  public postCreaProducto(producto: Producto): Observable<Producto> {
    return this.http.post<Producto>(this.urlCreaproducto, producto, {
      headers: this.headers,
    });
  }


  /*public postCreaUsuario(data) {
    this.http
      .post<Usuario>(this.urlCreausuario, data, {
        headers: this.headers,
      })
      .subscribe(
        (r) => {
          // console.log(JSON.stringify(this.EncryptDecryptService.encrypt(r)));
          var encriptado = this.EncryptDecryptService.encrypt(
            JSON.stringify(r)
          );
          var desencriptado = this.EncryptDecryptService.decrypt(encriptado);
          //console.log(desencriptado);

          localStorage.setItem('usuarioAdmin', encriptado);
          this.setLoading(false);
          this.router.navigateByUrl('admin/home-admin');
        },
        (err) => {
        }
      );
  }*/

}
