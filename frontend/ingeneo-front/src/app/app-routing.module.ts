import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrearcuentaComponent } from './crearcuenta/crearcuenta.component';
import { CrearempresaComponent } from './crearempresa/crearempresa.component';
import { CrearproductoComponent } from './crearproducto/crearproducto.component';
import { IniciosesionComponent } from './iniciosesion/iniciosesion.component';
import { InicioComponent } from './inicio/inicio.component';

const routes: Routes = [
  { path: '', redirectTo: '/inicio', pathMatch: 'full'},
  { path: 'inicio', component: InicioComponent },
  { path: 'iniciosesion', component: IniciosesionComponent },
  { path: 'crearcuenta', component: CrearcuentaComponent },
  { path: 'crearempresa', component: CrearempresaComponent },
  { path: 'crearproducto', component: CrearproductoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
