import { Component } from '@angular/core';
import { ConfigService } from './config.service';
import { Router, NavigationEnd } from '@angular/router';

declare let gtag: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Logística Ingeneo';

  protected Server = ConfigService.settings.webServer;

  constructor(public router: Router) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // console.log(event.urlAfterRedirects);
        window['gtag']('config', this.Server.gtag, {
          page_path: event.urlAfterRedirects,
        });
      }
    });
  }
}
