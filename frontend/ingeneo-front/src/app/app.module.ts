import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER  } from '@angular/core';

import { AppComponent } from './app.component';
import { CrearcuentaComponent } from './crearcuenta/crearcuenta.component';
import { AppRoutingModule } from './app-routing.module';
import { IniciosesionComponent } from './iniciosesion/iniciosesion.component';
import { InicioComponent } from './inicio/inicio.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CrearempresaComponent } from './crearempresa/crearempresa.component';
import { CrearproductoComponent } from './crearproducto/crearproducto.component';
import { ConfigService } from './config.service';

export function initializeApp(configService: ConfigService) {
  return (): Promise<any> => {
    return configService.load();
  };
}

@NgModule({
  declarations: [
    AppComponent,
    CrearcuentaComponent,
    IniciosesionComponent,
    InicioComponent,
    CrearempresaComponent,
    CrearproductoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [ConfigService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
