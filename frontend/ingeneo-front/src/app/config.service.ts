import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  static settings: IAppConfig;

  constructor(private http: HttpClient) { }

  load(){

    const jsonFile = 'assets/config.json';
     return new Promise<void>((resolve, reject) => {
       this.http
         .get(jsonFile)
         .toPromise()
         .then((response: IAppConfig) => {
          ConfigService.settings = <IAppConfig>response;

            console.log('Config Loaded');
            console.log(ConfigService.settings);
           localStorage.setItem('baseurl', ConfigService.settings.webServer.baseurl );
           resolve();
         })
         .catch((response: any) => {
           reject(`Could not load the config file`);
         });
     });

  }

}

export interface IAppConfig {
  env: {
    name: string;
  };

  webServer: {
    baseurl: string;
    gtag: string;
  };
}
