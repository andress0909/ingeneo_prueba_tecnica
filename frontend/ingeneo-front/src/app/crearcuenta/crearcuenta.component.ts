import { Component, OnInit } from '@angular/core';
import { Usuario } from '../models/usuario';
import { ApiservicesService } from 'src/app/apiservices.service';
import { HttpClient } from '@angular/common/http';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';


@Component({
  selector: 'app-crearcuenta',
  templateUrl: './crearcuenta.component.html',
  styleUrls: ['./crearcuenta.component.css']
})
export class CrearcuentaComponent implements OnInit {
  mensaje='';
  public usuario: Usuario = {    
    identificacion: null,    
    nombres: null,
    apellidos: null,
    correo: null,
    celular: null,
    usuario: null,  
    contraseña: null,
  };
  constructor(
    private http: HttpClient,
    private apiservicea: ApiservicesService,
  ) { }

  myForm = new FormGroup({
    identificacion: new FormControl('', [Validators.required]),
    nombres: new FormControl('', [Validators.required]),
    apellidos: new FormControl('', [Validators.required]),
    correo: new FormControl('', [Validators.required]),
    celular: new FormControl('', [Validators.required]),
    usuario: new FormControl('', [Validators.required]),
    contrasena: new FormControl('', [Validators.required]),
  });

  submit() {
    console.log("inicio");
    let usuario = new Usuario();
    usuario.identificacion = this.myForm.controls.identificacion.value;
    usuario.nombres = this.myForm.controls.nombres.value;
    usuario.apellidos = this.myForm.controls.apellidos.value;
    usuario.correo = this.myForm.controls.correo.value;
    usuario.celular = this.myForm.controls.celular.value;
    usuario.usuario = this.myForm.controls.usuario.value;
    usuario.contraseña = this.myForm.controls.contrasena.value;

    this.apiservicea.postCreaUsuario(usuario).subscribe(
      (data) => {
        //this.alertService.success('Se creó satisfactoriamente la Noticia ');
        //this.apiadminservice.setLoading(false);
        usuario=data[0];
        this.mensaje='Usuario creado!! '+usuario.usuario;
      },
      (err) => {
        this.mensaje='No se creó el usuario!! ';
        //this.apiadminservice.setLoading(false);
      }
    );
    this.myForm.reset();
  }

  ngOnInit(): void {
  }

}
