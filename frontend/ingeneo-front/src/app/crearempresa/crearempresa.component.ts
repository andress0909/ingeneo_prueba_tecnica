import { Component, OnInit } from '@angular/core';
import { Empresa } from '../models/empresa';
import { ApiservicesService } from 'src/app/apiservices.service';
import { HttpClient } from '@angular/common/http';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-crearempresa',
  templateUrl: './crearempresa.component.html',
  styleUrls: ['./crearempresa.component.css']
})
export class CrearempresaComponent implements OnInit {
  mensaje='';
  public empresa: Empresa = {    
    nit: null,    
    nombre: null,
    correo: null,
    direccion: null,
    telefono: null,      
  };
  constructor(
    private http: HttpClient,
    private apiservicea: ApiservicesService,
  ) { }

  myForm = new FormGroup({
    nit: new FormControl('', [Validators.required]),
    nombre: new FormControl('', [Validators.required]),
    correo: new FormControl('', [Validators.required]),
    direccion: new FormControl('', [Validators.required]),
    telefono: new FormControl('', [Validators.required]),
  });

  submit() {

    let empresa = new Empresa();
    empresa.nit = this.myForm.controls.nit.value;
    empresa.nombre = this.myForm.controls.nombre.value;
    empresa.correo = this.myForm.controls.correo.value;
    empresa.direccion = this.myForm.controls.direccion.value;
    empresa.telefono = this.myForm.controls.telefono.value;
    

    this.apiservicea.postCreaEmpresa(empresa).subscribe(
      (data) => {
        empresa=data[0];
        this.mensaje='Empresa creada!! '+empresa.nombre;
        //this.alertService.success('Se creó satisfactoriamente la Noticia ');
        //this.apiadminservice.setLoading(false);
      },
      (err) => {
        this.mensaje='No se creó la empresa!! ';
        //this.apiadminservice.setLoading(false);
      }
    );
    this.myForm.reset();
  }

  ngOnInit(): void {
  }

}
