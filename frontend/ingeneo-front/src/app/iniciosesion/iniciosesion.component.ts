import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ApiservicesService } from 'src/app/apiservices.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-iniciosesion',
  templateUrl: './iniciosesion.component.html',
  styleUrls: ['./iniciosesion.component.css']
})
export class IniciosesionComponent implements OnInit {

  myForm = new FormGroup({
    usuario: new FormControl('', [Validators.required]),
    contrasena: new FormControl('', [Validators.required]),
  });

  mensaje='';

  constructor(
    private readonly fb: FormBuilder,
    private http: HttpClient,
    private apiservicea: ApiservicesService,
  ) {  }

  
  ngOnInit(): void {
  }

}
