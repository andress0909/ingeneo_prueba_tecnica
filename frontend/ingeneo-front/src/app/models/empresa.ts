export class Empresa {
    nit: String;    
    nombre: String;
    correo: String;
    direccion: String;
    telefono: String;    
}