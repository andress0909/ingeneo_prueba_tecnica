export class Producto {
    idPro: Number;    
    nombre: String;
    descripcion: String;
    precio: Number;
}