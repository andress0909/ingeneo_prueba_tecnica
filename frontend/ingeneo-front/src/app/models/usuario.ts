export class Usuario {
    identificacion: String;    
    nombres: String;
    apellidos: String;
    correo: String;
    celular: String;
    usuario: String;  
    contraseña: String;      
}